using System.Diagnostics;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement; //libreria para poder reiniciar la escena
using UnityEngine;


public class ControlJugador : MonoBehaviour 

{
    public Camera camaraPrimeraPersona;
    public float rapidezDesplazamiento = 10.0f;
    public GameObject proyectil;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private bool dobleSalto;
    private Rigidbody rb;
    
    


    void Start() 
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update() { 

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento; 
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento; 



        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;


        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras); 

        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; 
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
               

                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));

                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaņo();
                    }
                }
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);

            Destroy(pro, 5);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso())
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            }
            else if (dobleSalto)
            {
                rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
                rb.velocity = Vector3.zero;
                dobleSalto = false;
            }
        }

        if (EstaEnPiso())
        {
            dobleSalto = true;  //se activa el doble salto cuando el jugador esta en el piso
        }

        if (Input.GetKey(KeyCode.R)) { SceneManager.LoadScene(0); Time.timeScale = 1; } // si se toca la R se reinicia la escena

        
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)   // cuando el jugador toca un "coleccionable" este se desactiva
        {


            other.gameObject.SetActive(false);
            rapidezDesplazamiento += 2;

            transform.localScale = new Vector3(transform.localScale.x /2 +1, transform.localScale.y / 2 +1, transform.localScale.z / 2 +1);



        }
        if (other.gameObject.CompareTag("MapaFin") == true)
        {
            SceneManager.LoadScene(0);
            
        }
    }

    
}

