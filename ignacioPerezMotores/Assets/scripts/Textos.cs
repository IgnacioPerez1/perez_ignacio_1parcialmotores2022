using System.Collections;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using System.Diagnostics;
using UnityEngine;

public class Textos : MonoBehaviour
{
    private float tiempo = 60f;
    public int rapidez;
    public TMPro.TMP_Text TextoObtenido;
    public TMPro.TMP_Text TextoGanaste;
    public TMPro.TMP_Text Timer;
    public TMPro.TMP_Text TextoDeLingotesFaltantes;
    public TMPro.TMP_Text Perdiste;

    private int cont;
    private int lingIniciales;

    // Start is called before the first frame update
    void Start()
    {
        {

            cont = 0;
            lingIniciales = 7;
            TextoGanaste.text = "";
            Perdiste.text = "";
            setearTextos();

            Timer.text = "Tiempo restante: " + tiempo;
            StartCoroutine(reloj(60));
        }

    }
    private void setearTextos()
    {
        TextoObtenido.text = "Lingotes obtenidos:" + cont.ToString();
        TextoDeLingotesFaltantes.text = "Lingotes faltantes:" + lingIniciales;
        Timer.text = "TIEMPO: " + tiempo;
        if (cont >= 7)
        {
            Timer.text = "";
            TextoGanaste.text = "GANASTE";
            TextoObtenido.text = "";
            TextoDeLingotesFaltantes.text = "";
            Time.timeScale = 0;
            if (Input.GetKey(KeyCode.R)) { Time.timeScale = 1; }
        }

        if (tiempo <= 1)
            {
            
                Timer.text = "";
                Perdiste.text = "PERDISTE :(";
                TextoObtenido.text = "";
                TextoDeLingotesFaltantes.text = "";
                Time.timeScale = 0;
                if(Input.GetKey(KeyCode.R)) { Time.timeScale = 1; }
        }

    }


    void FixedUpdate() {


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            cont = cont + 1;
            lingIniciales = lingIniciales - 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        
    }
    private IEnumerator reloj(float cronometro = 60)
    {
        tiempo = cronometro;
        while (tiempo > 0)
        {
            setearTextos();
            
            yield return new WaitForSeconds(1.0f);
            tiempo--;
        }
        
        
    }
}


